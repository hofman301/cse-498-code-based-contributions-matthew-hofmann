# This is a sample Python script.
import datetime
# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


import random

def randomtimes(start, end, n):
    frmt = '%Y-%m-%d %H:%M:%S'
    stime = datetime.datetime.strptime(start, frmt)
    etime = datetime.datetime.strptime(end, frmt)
    td = etime - stime
    return [random.random() * td + stime for _ in range(n)]


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    random_datetime = randomtimes("2022-1-1 1:30:00", "2022-12-31 04:50:34", 929)
    for i in random_datetime:
     print(" "+i.strftime('%Y-%m-%d %H:%M:%S'))

    print()
    print("End Time stamps")
    print()
    print()
    for i in random_datetime:
        a = i + datetime.timedelta(minutes = 20)
        print(" " + a.strftime('%Y-%m-%d %H:%M:%S'))

